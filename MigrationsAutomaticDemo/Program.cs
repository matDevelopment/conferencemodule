﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MigrationsAutomaticDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new Model.RoomContext())
            {
                db.Rooms.Add(new Model.Room { Name = "Sala 222" });
                db.SaveChanges();

                foreach (var room in db.Rooms)
                {
                    Console.WriteLine(room.Name);
                }
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
    
}
