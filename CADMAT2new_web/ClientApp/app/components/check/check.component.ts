﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { CheckService } from './../../services/check.service';
import { PresenceResource } from './../../resources/PresenceResource';
import { Timestamp } from 'rxjs/operator/timestamp';
import { RoomResource } from '../../resources/RoomResource';
import { GuestResource } from '../../resources/GuestResource';

@Component({
    selector: 'check',
    templateUrl: './check.component.html'
})
export class CheckComponent implements OnInit{
    //guestId: number;
    //roomId: number;
    presence: PresenceResource = new PresenceResource();
    room: RoomResource = new RoomResource(); 
    guest: GuestResource = new GuestResource();
    id: number;
   
    constructor(private checkService: CheckService,
        private router: Router,
        private route: ActivatedRoute) {

        this.presence.SubmitTime = Date.now();
        
               
    }
    ngOnInit() {
       
        this.Add();
    }
    private  Add() {        
        //this.getGuest();
        //this.getRoom();
        this.presence.GuestId = +this.route.snapshot.params['guestId'];
        this.presence.RoomId = +this.route.snapshot.params['roomId'];
        console.log(this.presence);
        this.checkService.AddCheck(this.presence).subscribe(result => {
            this.presence = result;
        });
    }

    private getRoom() {
        let id = +this.route.snapshot.params['roomId'];            
            this.checkService.GetRoom(id).subscribe(response => {
                console.log(response);
                this.room = response;
            })

        
    }
    private getGuest() {
        let id = +this.route.snapshot.params['guestId'];
        this.checkService.GetGuest(id).subscribe(response => {
            console.log(response);
            this.guest = response;
        })


    }
}
