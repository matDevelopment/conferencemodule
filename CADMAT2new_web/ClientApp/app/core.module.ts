﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './components/app/app.component';
import { CheckService } from './services/check.service';
import { CadmatService } from './services/cadmat.service';


@NgModule({
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        
    ],
    
       
    providers: [
        { provide: 'BASE_URL', useFactory: getBaseUrl }, CheckService, CadmatService
    ]
})
export class CoreModule {
}

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
