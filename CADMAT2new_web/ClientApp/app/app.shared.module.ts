import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { CheckComponent } from './components/check/check.component';
import { CoreModule } from './core.module';
import { QrIdComponent} from './components/QrId/QrId.component'
import { LoginComponent } from './components/login/login.component'
import { RegisterComponent} from'./components/register/register.component'

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent,
        CheckComponent,
        QrIdComponent,
        LoginComponent,
        RegisterComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        CoreModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: 'check/:guestId/:roomId', component: CheckComponent },
            { path: 'QrId/:idNumber', component: QrIdComponent },
            { path: 'login/:firstName/:surname', component: LoginComponent },
            { path: 'register/:firstName/:surname/:company/:sesion/:email', component: RegisterComponent}

        ])
    ],
    
})
export class AppModuleShared {
}
