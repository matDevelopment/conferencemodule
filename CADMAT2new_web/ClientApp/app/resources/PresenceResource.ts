﻿import { GuestResource } from './GuestResource';
import { RoomResource } from './RoomResource';

export class PresenceResource {
    Id: number;
    //Guest: GuestResource = new GuestResource();
    //Room: RoomResource = new RoomResource();
    GuestId: number;
    RoomId: number;
    SubmitTime: number;

}