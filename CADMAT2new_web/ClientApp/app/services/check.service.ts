﻿import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from "@angular/http";
import { Subject } from "rxjs/Subject";
import { PresenceResource } from './../resources/PresenceResource';

@Injectable()
export class CheckService {
    actionActivated = new Subject();
    constructor(private http: Http) { }

    AddCheck(newCheck: PresenceResource) {
       return this.http.put("/api/check/new", newCheck)
            .map(r => r.json())
            .catch((error: any) => {
                alert(error._body);
                return Observable.throw(new Error(error.status));
            })
    }

    GetRoom(id: number) {
        return this.http.get("/api/room/" + id)
            .map(r => r.json())
            .catch((error: any) => {
                alert(error._body);
                return Observable.throw(new Error(error.status));
            })
    }
    GetGuest(id: number) {
        return this.http.get("/api/guest/" + id)
            .map(r => r.json())
            .catch((error: any) => {
                alert(error._body);
                return Observable.throw(new Error(error.status));
            })
    }
}