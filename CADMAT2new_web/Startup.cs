using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CADMAT2new_Domain.Migrations;
using Newtonsoft.Json.Serialization;
using NLog.Extensions.Logging;
using NLog.Web;
using System.Globalization;
using CADMAT2new_Domain.Contexts;
using CADMAT2new_Domain;
using CADMAT2new_Domain.IRepositories;
using CADMAT2new_Domain.Repositories;
using CADMAT2new.Services;
using AutoMapper;
using CADMAT2new.DomainResources;
using CADMAT2new_Domain.Models;


namespace CADMAT2new
{
    public class Startup
    {



        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.{Environment.MachineName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            env.ConfigureNLog("nlog.config");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connString = Configuration["ConnectionStrings:DefaultConnection"];
            // Add framework services.
            services.Configure<IISOptions>(options => options.AutomaticAuthentication = true);
            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            // services.AddScoped<CADMAT2new_Domain.Contexts.AppContext>(_ => new CADMAT2new_Domain.Contexts.AppContext(Configuration.GetConnectionString("DefaultConnection")));
             services.AddScoped<CADMAT2new_Domain.Contexts.AppContext>(_ => new CADMAT2new_Domain.Contexts.AppContext(connString));
            services.AddTransient<IDataRepository, CADMAT2new_Domain.Contexts.AppRepository>();

            services.AddTransient<ICheckRepository, CheckRepository>();
            services.AddTransient<ICadMatRepository, CadMatRepository>();

            services.AddTransient<ICheckService, CheckService>();
            services.AddTransient<ICadMatService, CadMatService>();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddNLog();
            app.AddNLogWeb();

            if (env.IsDevelopment())
            {
                CADMAT2new_Domain.Migrations.Configuration.SeedData = true;

                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else if (env.IsStaging())
            {
                CADMAT2new_Domain.Migrations.Configuration.SeedData = true;
                app.UseDeveloperExceptionPage();
            }
            else
            {
                CADMAT2new_Domain.Migrations.Configuration.SeedData = true;
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                // routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


                routes.MapRoute(
                     "Qr Id number",
                     "QrId/{idNumber}",
                     new { controller = "QrId", action = "QrId", idNumber = "000000" }
                );

                routes.MapRoute(
                   name: "check",
                   template: "{ controller = Check}/{action = Check}/{guestid?}/{roomid?}"
               //new { controller = "Check", action = "check", idNumber = "0", guestid = "0"}
               );

                routes.MapRoute(
                    "Logowanie",
                    "Login/{imie}/{nazwisko}",
                    new { controller = "Login", action = "Login", nazwa = "TEST" }
               );

                routes.MapRoute(
                   "Rejstracja",
                   "Rejstracja/{imie}/{nazwisko}/{firma}/{sesja}/{email}",
                   new { controller = "Rejstracja", action = "Rejstracja", nazwa = "TEST" }
              );

                routes.MapRoute(
                   "Seria",
                   "Seria/{skip}/{take}",
                   new { controller = "QrId", action = "Seria", nazwa = "TEST" }
              );
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
            InitiailizeAutomapper();
        }
        private void InitiailizeAutomapper()
        {
            Mapper.Initialize(cfg =>
            {
                ConfigureAutomapper(cfg);
            });
        }
        public static void ConfigureAutomapper(IMapperConfigurationExpression cfg)
        {
            const string format = "yyyy-MM-ddTHH:mm:ss.fffZ";

            cfg.CreateMap<PresenceResource, Presence>();
                //.ForMember(dest => dest.GuestId, opt => opt.MapFrom(src => src.Guest.Id))
                //.ForMember(dest => dest.Guest, opt => opt.Ignore())
                //.ForMember(dest => dest.RoomId, opt => opt.MapFrom(src => src.Room.Id))
                //.ForMember(dest => dest.Room,  opt => opt.Ignore());
            cfg.CreateMap<Presence, PresenceResource>();

            cfg.CreateMap<Room, RoomResource>();
            cfg.CreateMap<RoomResource, Room>();

            cfg.CreateMap<Guest, GuestResource>();
            cfg.CreateMap<GuestResource, Guest>();

            cfg.CreateMap<CadMatGuest, CadMatGuestResource>();
            cfg.CreateMap<CadMatGuestResource, CadMatGuest>();
        }
    }
}
