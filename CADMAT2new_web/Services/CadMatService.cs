﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CADMAT2new_Domain.Models;
using CADMAT2new_Domain.IRepositories;
using CADMAT2new_Domain.Contexts;
using CADMAT2new_Domain;

namespace CADMAT2new.Services
{
    public class CadMatService: ICadMatService
    {
        private ICadMatService cadMatRepository;
        private IDataRepository dataRepository;

        public CadMatService(ICadMatService cadMatRepository, IDataRepository repository)
        {
            this.cadMatRepository = cadMatRepository;
            this.dataRepository = repository;
        }
    }
}
