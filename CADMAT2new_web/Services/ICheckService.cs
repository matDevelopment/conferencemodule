﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CADMAT2new.DomainResources;

namespace CADMAT2new.Services
{
    public interface ICheckService
    {
        PresenceResource AddNewCheck(PresenceResource presence);
        PresenceResource GetCheck(int presenceId);
        RoomResource GetRoom(int roomId);
        GuestResource GetGuest(int guestId);
    }
}
