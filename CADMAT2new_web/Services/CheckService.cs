﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CADMAT2new_Domain;
using CADMAT2new_Domain.Models;
using CADMAT2new_Domain.IRepositories;
using CADMAT2new.DomainResources;
using AutoMapper;

namespace CADMAT2new.Services
{
    public class CheckService : ICheckService
    {
        private ICheckRepository checkRepository;
        private IDataRepository dataRepository;

        public CheckService(ICheckRepository checkRepository, IDataRepository repository)
        {
            this.checkRepository = checkRepository;
            this.dataRepository = repository;
        }
        public PresenceResource AddNewCheck(PresenceResource newPresence)
        {
            return Mapper.Map<Presence, PresenceResource>(checkRepository.AddNewCheck(Mapper.Map<PresenceResource, Presence>(newPresence)));
            
        }

        public PresenceResource GetCheck(int presenceId)
        {
            return Mapper.Map<Presence, PresenceResource>(checkRepository.GetCheck(presenceId));
        }

        public RoomResource GetRoom(int roomId)
        {
            return Mapper.Map<Room, RoomResource>(checkRepository.GetRoom(roomId));
        }

          public GuestResource GetGuest(int guestId)
        {
            return Mapper.Map<Guest, GuestResource>(checkRepository.GetGuest(guestId));
        }
    }
}
