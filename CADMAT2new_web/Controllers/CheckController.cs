﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CADMAT2new_Domain.Contexts;
using CADMAT2new.DomainResources;
using CADMAT2new.Services;

namespace CADMAT2new.Controllers
{
   
    public class CheckController : Controller
    {
        private ICheckService _service;
        private readonly ILogger<CheckController> _logger;

        public CheckController(ICheckService service, ILogger<CheckController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpPut("api/check/new")]
        public IActionResult Check([FromBody]  PresenceResource newPresence)
        {

            try
            {
                var newlyCreatedPresence = _service.AddNewCheck(newPresence);
                return Ok(newlyCreatedPresence);
            }
            catch (Exception exe)
            {
                ModelState.AddModelError("general error", "Error during createion of presence" + exe.Message);
                _logger.LogInformation("Attempt to create presence");

                return BadRequest("Error during createion  of presence:  " + exe.Message);
            }


        }

        [HttpGet("/api/room/{RoomId}")]
        public IActionResult GetRoom(int RoomId)
        {
            try
            {                 
                return Ok(_service.GetRoom(RoomId));
            }
            catch (Exception exe)
            {
                throw exe;
            }
        }
        [HttpGet("/api/guest/{GuestId}")]
        public IActionResult GetGuest(int GuestId)
        {
            try
            {
                return Ok(_service.GetGuest(GuestId));
            }
            catch (Exception exe)
            {
                throw exe;
            }
        }

    }
}