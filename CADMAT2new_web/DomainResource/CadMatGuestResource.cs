﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CADMAT2new.DomainResources
{
    public class CadMatGuestResource: EntityResource
    {
       public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public string Sesion { get; set; }
        public string Email { get; set; }
        
    }
}
