﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CADMAT2new.DomainResources
{
    public class PresenceResource : EntityResource
    {

        public int Id { get; set; }
        public int GuestId { get; set; }
        public GuestResource Guest { get; set; }
        public int RoomId { get; set; }
        public RoomResource Room { get; set; }
        public long SubmitTime { get; set; }
    }
}
