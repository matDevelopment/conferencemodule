﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CADMAT2new_Domain.Models;


namespace CADMAT2new_Domain
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new Model.RoomContext())
            {
                db.Rooms.Add(new Model.Room { Name = "Sala 222" });
                db.SaveChanges();

                foreach (var room in db.Rooms)
                {
                    Console.WriteLine(room.Name);
                }
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
