﻿using System;
using CADMAT2new_Domain.Models;

namespace CADMAT2new_Domain
{
    public interface IDataRepository
    {
       
        T DeleteEntity<T>(T entity) where T : Entity;
    }
}
