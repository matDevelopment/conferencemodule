﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace CADMAT2new_Domain.Models
{
    public abstract class Entity
    {
        public Boolean IsVisible { get; set; } = true;

        public virtual void Delete()
        {
            this.IsVisible = false;
        }

        public void CloneFrom(Entity source, string[] ignorelist = null)
        {
            var paramentersToIgnore = new List<string>() { "Id" };//never clone id
            if (ignorelist != null)
            {
                paramentersToIgnore.AddRange(ignorelist);
            }
            if (source == null)
            {
                return;
            }

            Type sourceType = source.GetType();
            if (this.GetType() != sourceType)
            {
                throw new ApplicationException("Can not clone: type not match");
            }
            //This will only use public properties.Is that enough?
            foreach (PropertyInfo propertyInfo in sourceType.GetProperties())
            {
                if (propertyInfo.CanRead)
                {
                    object sourceValue = propertyInfo.GetValue(source, null);
                    object thisValue = propertyInfo.GetValue(this, null);
                    if (IsPropertyToClone(propertyInfo, sourceValue, thisValue, paramentersToIgnore))
                    {
                        propertyInfo.SetValue(this, sourceValue);
                    }
                }
            }
        }

        private bool IsPropertyToClone(PropertyInfo propertyInfo, object sourceValue, object thisValue, IEnumerable<string> ignoreList)
        {
            if (ignoreList.Contains(propertyInfo.Name))
            {
                return false;
            }
            if (object.Equals(sourceValue, thisValue))
            {
                return false;
            }
            if (propertyInfo.PropertyType.BaseType != null && propertyInfo.PropertyType.BaseType.Name == "ValueType")
            {
                return true;
            }
            if (propertyInfo.PropertyType == typeof(String))
            {
                return true;
            }
            return false;
        }
    }
}
