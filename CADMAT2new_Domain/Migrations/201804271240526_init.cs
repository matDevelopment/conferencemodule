namespace CADMAT2new_Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Guest",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsVisible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Presence",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GuestId = c.Int(nullable: false),
                        RoomId = c.Int(nullable: false),
                        SubmitTime = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Guest", t => t.GuestId)
                .ForeignKey("dbo.Room", t => t.RoomId)
                .Index(t => t.GuestId)
                .Index(t => t.RoomId);
            
            CreateTable(
                "dbo.Room",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsVisible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Presence", "RoomId", "dbo.Room");
            DropForeignKey("dbo.Presence", "GuestId", "dbo.Guest");
            DropIndex("dbo.Presence", new[] { "RoomId" });
            DropIndex("dbo.Presence", new[] { "GuestId" });
            DropTable("dbo.Room");
            DropTable("dbo.Presence");
            DropTable("dbo.Guest");
        }
    }
}
