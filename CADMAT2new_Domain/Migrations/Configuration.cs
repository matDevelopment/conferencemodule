﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using CADMAT2new_Domain.Helpers;

namespace CADMAT2new_Domain.Migrations
{
    public sealed class Configuration: DbMigrationsConfiguration<CADMAT2new_Domain.Contexts.AppContext>
    {
        public static bool SeedData { get; set; } = true;

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(CADMAT2new_Domain.Contexts.AppContext context)
        {
            if(SeedData)
            {
                var dataInitializer = new DataInitailizeHelper();
                dataInitializer.SetSampleData(context);
            }
        }
    }

}
