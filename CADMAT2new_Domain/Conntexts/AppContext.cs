﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using CADMAT2new_Domain.Models;
using CADMAT2new_Domain.Migrations;
using CADMAT2new_Domain.SQLRepository;



namespace CADMAT2new_Domain.Contexts
{
    public class AppContext : DbContext
    {
        public static string ConnectionString { get; set; }

        public AppContext(string connString) : base(connString)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppContext, Configuration>());
        }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Presence> Presences { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<PLMContext, Configuration>());
            //Database.Initialize(true);

        }
        public class AppContextFactory : IDbContextFactory<AppContext>
        {
            private ConnectionStringConfig config = ConnectionStringConfig.ReadFromConnectionStringConfigFile();
            public AppContext Create()
            {
                if (String.IsNullOrEmpty(AppContext.ConnectionString))
                {
                    config = ConnectionStringConfig.ReadFromConnectionStringConfigFile();
                    AppContext.ConnectionString = config.ConnectionString;
                }
                return new AppContext(AppContext.ConnectionString);

            }
        }
    }
}
