﻿using System;
using System.Collections.Generic;
using System.Text;
using CADMAT2new_Domain.Models;

namespace CADMAT2new_Domain.Contexts
{
    public class AppRepository : IDataRepository
    {
        private AppContext context;

        public AppRepository(AppContext _context)
        {
            context = _context;
        }

       

        public T DeleteEntity<T>(T entity) where T : Entity
        {
            var dbEntity = context.Entry<T>(entity).Entity;
            dbEntity.Delete();
            this.context.Entry(dbEntity).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
            return dbEntity;
        }
    }
}
