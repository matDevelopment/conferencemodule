﻿using System;
using System.Collections.Generic;
using System.Text;
using CADMAT2new_Domain.Contexts;
using CADMAT2new_Domain.Migrations;
using CADMAT2new_Domain.Models;
using System.Data.Entity;

namespace CADMAT2new_Domain.Contexts
{
    class AppDbInitializer : MigrateDatabaseToLatestVersion<AppContext, Configuration>
    {

        #region DataCollection
        IList<Guest> defaultGuests = new List<Guest>();
        IList<Room> defaultRooms = new List<Room>();
        IList<Presence> defaultPresences = new List<Presence>();
        #endregion
    }
}
