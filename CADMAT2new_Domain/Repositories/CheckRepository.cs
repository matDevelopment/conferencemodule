﻿using System;
using System.Collections.Generic;
using System.Text;
using CADMAT2new_Domain.Models;
using CADMAT2new_Domain.IRepositories;
using CADMAT2new_Domain.Contexts;
using System.Linq;


namespace CADMAT2new_Domain.Repositories
{
    public class CheckRepository : ICheckRepository
    {
        private Contexts.AppContext context;

        public CheckRepository(Contexts.AppContext _context)
        {
            context = _context;
        }

        public Presence AddNewCheck(Presence presence)
        {
          
            context.Presences.Add(presence);
            context.SaveChanges();
            presence = GetCheck(presence.Id);
            return presence;
        }
        public Presence GetCheck(int presenceId)
        {
            return context.Presences
                .Single(x => x.Id == presenceId);
        }
       public Room GetRoom(int roomId)
        {
            return context.Rooms
                .Single(x => x.Id == roomId);
        }
        public Guest GetGuest(int guestId)
        {
            return context.Guests
                .Single(x => x.Id == guestId);
        }
    }
}
