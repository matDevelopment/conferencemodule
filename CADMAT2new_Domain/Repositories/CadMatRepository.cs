﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CADMAT2new_Domain.Models;
using CADMAT2new_Domain.IRepositories;
using CADMAT2new_Domain.Contexts;
using Word = Microsoft.Office.Interop.Word;
using System.Globalization;
using System.Threading;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CADMAT2new_Domain.Repositories
{
    public class CadMatRepository: ICadMatRepository
    {
        private Contexts.AppContext context;

        public CadMatRepository(Contexts.AppContext _context)
        {
            context = _context;
        }
        string[] sesje = new string[] { "sesja1", "sesja2", "sesja3", "sesja4", "sesja5", "sesja6", "sesja7", "sesja8", "sesja9", };
        public int link = new int();

        //CadMatGuest GetGuestByName(string surname, string name)
        //{
        //    name = name.Trim();
        //    surname = surname.Trim();
        //    string nazwa = name + " " + surname;

        //    int result = context.WpmatCf7dbpluginSubmits
        //                .OrderByDescending(t1 => t1.SubmitTime)
        //                .Where(t1 => t1.FieldOrder == 0)
        //                .Where(t1 => t1.FormName == "Form-CADMAT")
        //                .Where(t1 => t1.FieldValue.Trim() == nazwa)
        //                .Select(t1 => t1.SubmitTime)
        //                .FirstOrDefault();
        //             return   GetGuestById(result);
            
        //}

        //CadMatGuest GetGuestById(int Id)
        //{
        //    CadMatGuest cadMatGuest = context.WpmatCf7dbpluginSubmits.OrderBy(p => p.SubmitTime).Where(p => p.SubmitTime == Id).Any());
        //    PrintGuest(cadMatGuest);
        //    return cadMatGuest;
        //}

        //void PrintGuest(CadMatGuest guest)
        //{
        //    decimal id = decimal.Round(guest.Id, 4);
        //    if (context.WpmatCf7dbpluginSubmits.Where(x => x.SubmitTime == id).Any() &&
        //                    !context.WpmatCf7dbpluginSubmits.Where(x => x.SubmitTime == id && x.FieldOrder == 100).Any())
        //    {

        //        var username = context.WpmatCf7dbpluginSubmits.OrderBy(x => x.SubmitTime)
        //                        .Where(x => x.SubmitTime == id && x.FieldOrder == 0)
        //                        .Select(x => x.FieldValue)
        //                        .FirstOrDefault();
        //        var firma = context.WpmatCf7dbpluginSubmits.OrderBy(x => x.SubmitTime)
        //            .Where(x => x.SubmitTime == id && x.FieldOrder == 3)
        //            .Select(x => x.FieldValue)
        //            .FirstOrDefault();
        //        var sesja = context.WpmatCf7dbpluginSubmits.OrderBy(x => x.SubmitTime)
        //            .Where(x => x.SubmitTime == id && x.FieldOrder == 10)
        //            .Select(x => x.FieldValue)
        //            .FirstOrDefault();

        //        if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(firma) || string.IsNullOrEmpty(sesja)) return;

        //        TextInfo textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;

        //        username = textInfo.ToTitleCase(username.Trim().ToLower());

        //        for (int i = 0; i < 9; i++)
        //            if (sesja.Contains(sesje[i]))
        //                link = i;


        //        PrintWordDocument(MyWordDocumentsManager.lista[link].wordApp, username, firma);


        //        var tmp = new WpmatCf7dbpluginSubmit
        //        {
        //            SubmitTime = id,
        //            FormName = "Form-CADMAT",
        //            FieldName = "​​obecny",
        //            FieldValue = "1",
        //            FieldOrder = 100,
        //            File = null,
        //        };

        //        context.Add(tmp);
        //        context.SaveChanges();


        //    }
        //}

        private void FindAndReplece(Microsoft.Office.Interop.Word.Application wordApp, object findText, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundLike = false;
            object matchAllForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiactitics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;


            wordApp.Selection.Find.Execute(ref findText,
                          ref matchCase, ref matchWholeWord,
                          ref matchWildCards, ref matchSoundLike,
                          ref matchAllForms, ref forward,
                          ref wrap, ref format, ref replaceWithText,
                          ref replace, ref matchKashida,
                          ref matchDiactitics, ref matchAlefHamza,
                          ref matchControl);

        }


        private void PrintWordDocument(object filename, string username, string firma)
        {
            object missing = Missing.Value;



            //Find and replace:
            this.FindAndReplece(MyWordDocumentsManager.lista[link].wordApp, "<imie-nazw>", username);
            this.FindAndReplece(MyWordDocumentsManager.lista[link].wordApp, "<firma>", firma);


            //print document
            #region Print Document :

            object copies = "1";
            object pages = "1";
            object range = Word.WdPrintOutRange.wdPrintCurrentPage;
            object items = Word.WdPrintOutItem.wdPrintDocumentContent;
            object pageType = Word.WdPrintOutPages.wdPrintAllPages;
            object oTrue = true;
            object oFalse = false;


            object nullobj = Missing.Value;

            MyWordDocumentsManager.lista[link].wordApp.Visible = true;

            {
                MyWordDocumentsManager.lista[link].aDoc.PrintOut(
                    ref oTrue, ref oFalse, ref range, ref missing, ref missing, ref missing,
                    ref items, ref copies, ref pages, ref pageType, ref oFalse, ref oTrue,
                    ref missing, ref oFalse, ref missing, ref missing, ref missing, ref missing);
            }
            #endregion



            MyWordDocumentsManager.lista[link].aDoc.Undo();
            MyWordDocumentsManager.lista[link].aDoc.Undo();
            // dla pewnosci
            MyWordDocumentsManager.lista[link].aDoc.Undo();
            MyWordDocumentsManager.lista[link].aDoc.Undo();
        }
    }
}
