﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;


namespace CADMAT2new_Domain
{
    public class MyWordDocument
    {
        Word.Document _aDoc = null;
        Word.Application _wordApp = null;

        object missing = Missing.Value;

        object path;

        public MyWordDocument(object filename)
        {
            path = filename;

            CreateWordDocument(filename);
        }

        private void CreateWordDocument(object filename)
        {
            _wordApp = new Word.Application();

            if (File.Exists((string)filename))
            {
                DateTime today = DateTime.Now;

                object readOnly = false;
                object isVisible = false;

                wordApp.Visible = false;

                _aDoc = wordApp.Documents.Open(ref filename, ref missing, ref readOnly,
                                                ref missing, ref missing, ref missing,
                                                ref missing, ref missing, ref missing,
                                                ref missing, ref missing, ref missing,
                                                ref missing, ref missing, ref missing, ref missing);

                _aDoc.Activate();
            }
        }

        public Word.Application wordApp
        {
            get
            {
                return _wordApp;
            }
        }




        public Word.Document aDoc
        {
            get
            {
                return _aDoc;
            }
        }

        public void Close()
        {
            if (_aDoc != null)
            {
                _aDoc.Close(Word.WdSaveOptions.wdDoNotSaveChanges);
                _aDoc = null;
            }

            if (_wordApp != null)
            {
                _wordApp.Quit(Word.WdSaveOptions.wdDoNotSaveChanges);
                _wordApp = null;
            }
        }
    }
}
