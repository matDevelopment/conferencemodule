﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CADMAT2new_Domain.Models;

namespace CADMAT2new_Domain.Helpers
{
    public class DataInitailizeHelper
    {
        IList<Guest> defaultGuests = new List<Guest>();
        IList<Room> defaultRooms = new List<Room>();


        private void setUpUsers()
        {
            defaultGuests.Add(new Guest() { Name = "Jan Kowalski",  IsVisible = true });
            defaultGuests.Add(new Guest() { Name = "Stanisław Nowak" , IsVisible = true });
            defaultGuests.Add(new Guest() { Name = "Jakub Wójcik",  IsVisible = true });
            defaultGuests.Add(new Guest() { Name = "Michał Kamiński", IsVisible = true });
        }

        private void setUpRooms()
        {
            defaultRooms.Add(new Room() {Name = "Room 102", IsVisible = true });
            defaultRooms.Add(new Room() {Name = "Room 205", IsVisible = true });
            defaultRooms.Add(new Room() {Name = "Room 359", IsVisible = true });
            defaultRooms.Add(new Room() {Name = "Room 153", IsVisible = true });

        }


        public void SetSampleData(CADMAT2new_Domain.Contexts.AppContext context)
        {
            setUpUsers();
            setUpRooms();

            context.Guests.AddRange(defaultGuests.Where(obj => !context.Guests.Any(contextObj => contextObj.Name == obj.Name)));
            context.Rooms.AddRange(defaultRooms.Where(obj => !context.Rooms.Any(contextObj => contextObj.Name == obj.Name)));
            context.SaveChanges();
        }
    }
}
