﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace CADMAT2new_Domain.SQLRepository
{
    public class ConnectionStringConfig
    {
        public static string EnvironmentName { get; set; } = Environment.MachineName;

        public static readonly string FileName = $"{EnvironmentName}-ConnectionStringConfig.xml";
        public static readonly string projpath = new Uri(Path.Combine(new string[]{System.AppDomain.CurrentDomain.BaseDirectory, "..\\.."})).AbsolutePath;
       // public static readonly string FilePath = $@"{projpath}\ConnectionStrings\{FileName}";
        //public static readonly string FilePath = $@"{AppDomain.CurrentDomain.BaseDirectory}\ConnectionStrings\{FileName}";
       public static readonly string FilePath = $@"C:\\Users\\Michał\\Source\\Repos\\CADMAT2new_web\\\CADMAT2new_Domain\\ConnectionStrings\{FileName}";
        
        public string ConnectionString { get; set; }
        public string ConnectionStringForTest { get; set; }

        public static ConnectionStringConfig ReadFromConnectionStringConfigFile()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ConnectionStringConfig));
            using (StreamReader reader = new StreamReader(FilePath))
            {
                return ((ConnectionStringConfig)serializer.Deserialize(reader));
            }
        }
    }
}
