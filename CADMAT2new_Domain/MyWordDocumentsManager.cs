﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CADMAT2new_Domain
{
    public class MyWordDocumentsManager
    {
        static List<MyWordDocument> _lista;

        public static List<MyWordDocument> lista
        {
            get
            {
                //linki do druku

                if (_lista == null || _lista.Count == 0)
                {
                    string assemblyPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                    _lista = new List<MyWordDocument>();

                    for (int i = 1; i <= 9; i++)
                        _lista.Add(new MyWordDocument(Path.Combine(assemblyPath, string.Format(@"identyfikatorsesja{0}.docx", i))));
                }

                return _lista;
            }
        }

        public static void Close()
        {
            if (_lista == null) return;

            foreach (var item in _lista)
            {
                item.Close();
            }

            _lista.Clear();
        }
    }
}
