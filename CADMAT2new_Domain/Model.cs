﻿using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;

namespace CADMAT2new_Domain
{
    class Model
    {
        public class RoomContext : DbContext
        {
            public DbSet<Room> Rooms { get; set; }
        }

        public class Room
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
