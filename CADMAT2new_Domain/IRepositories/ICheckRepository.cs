﻿using System;
using System.Collections.Generic;
using System.Text;
using CADMAT2new_Domain.Models;

namespace CADMAT2new_Domain.IRepositories
{
    public interface ICheckRepository
    {

        Presence AddNewCheck(Presence presence);
        Presence GetCheck(int presenceId);
        Room GetRoom(int roomId);
        Guest GetGuest(int guestId);
    }
}
